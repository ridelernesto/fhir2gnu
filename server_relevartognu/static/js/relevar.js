function data(item) {

	//console.log(item)
	//localStorage.setItem("data", item['identifier'][0]['value'])
	//window.location.href = "relevar_patient"
	//window.open('relevar_patient', '_blank');
	const URL = '/relevar_patient'
	const xhr = new XMLHttpRequest();
	sender = JSON.stringify(item)
	xhr.open('POST', URL);
	xhr.send(sender);
	xhr.addEventListener('readystatechange', function(e) {
      if( this.readyState === 4 ) {
        // the transfer has completed and the server closed the connection.
        console.log("finish")
        window.location.href = "relevar_patient"
        //window.open("relevar_patient");
      }
  	})
}

function save_data(item) {
	const URL = '/save_data'
	const xhr = new XMLHttpRequest();
	sender = JSON.stringify(item)
	xhr.open('POST', URL);
	xhr.send(sender);
	xhr.addEventListener('readystatechange', function(e) {
      if( this.readyState === 4 ) {
        // the transfer has completed and the server closed the connection.
        console.log("finish")
        window.location.href = "relevar"
        //window.open("relevar_patient");
      }
  })
	
}

function confirm_du(item){
	const URL = '/confirm_du'
	const xhr = new XMLHttpRequest();
	sender = JSON.stringify(item)
	xhr.open('POST', URL);
	xhr.send(sender);
	xhr.addEventListener('readystatechange', function(e) {
      if( this.readyState === 4 ) {
        // the transfer has completed and the server closed the connection.
        console.log("finish")
        window.location.href = "confirm_du"
      }
  }) 
}

function closeSession(){
	window.location.href = "relevar_login"
}

