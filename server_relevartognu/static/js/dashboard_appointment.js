function graph_line(x,y,id,title) {
    let mycanvas = document.getElementById(id).getContext("2d")

    var chart = new Chart(mycanvas, {
        type:"line",
        data:{
            labels: x,
            datasets:[
                {
                    label:title,
                    data:y,
                    borderWidth: 1,
                    borderColor: 'rgba(94,208,195,1)',
                    backgroundColor: 'rgba(94,208,195,0.5)'
                }
            ]
        },
        options:{
            maintainAspectRatio: false,
            scales: {
                    xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Mes'
                            }
                        }],
                    yAxes: [{
                            display: true,
                            ticks: {
                                beginAtZero: true,
                                steps: 10,
                                stepValue: 5
                            },
                            scaleLabel: {
                                display: true,
                            }
                        }]
                }
        }
    });

}

function graph_pie(categories, values, id, title){
    let mycanvas = document.getElementById(id).getContext("2d")

    var chart = new Chart(mycanvas, {
    type: 'pie',
    data:  {
        labels: categories,
        datasets: [
            {
                label:title,
                data: values,
                backgroundColor: [
                    "#5CA4BC",
                    "#43788A",
                    "#9EBDC8",
                    "#1E353D",
                    "#758C94",
                    "#5CA4BC",
                    "#43788A",
                    "#9EBDC8",
                    "#1E353D",
                    "#758C94"
                ]
            }]
        },
    options: {
    maintainAspectRatio: false,
    }
    });
}

function graph_radar(categories, values_men, values_woman, id, title){
    let mycanvas = document.getElementById(id).getContext("2d")

    var chart = new Chart(mycanvas, {
    type: 'radar',
    data:  {
        labels: categories,
        datasets: [
            {
                label:"F",
                data: values_woman,
                borderWidth: 1,
                borderColor: 'rgba(245,94,244,1)',
                backgroundColor: 'rgba(245,94,244,0.5)'
            },
            {
                label:"M",
                data: values_men,
                borderWidth: 1,
                borderColor: 'rgba(102,174,227,1)',
                backgroundColor: 'rgba(102,174,227,0.5)'
            }]
        },
    options: {
        maintainAspectRatio: false,
        scale: {     
                ticks: {
                  beginAtZero: true,
                  min: 0,
                  stepSize: 1
                }
        }
    }
    });
}