from flask_tryton import Tryton
import time
import json 
from datetime import datetime, date, timedelta
from decimal import Decimal, ROUND_UP

def create_patient(fhir_resource_patient, tryton_conection):
    # patient
    Patient = tryton_conection.pool.get('gnuhealth.patient')

    patient = Patient.search([('name.ref','=', fhir_resource_patient['identifier'][1]['value'])])

    # el paciente no existe debo crearlo
    if len(patient) == 0:
        new_patient = FHIR2GNUPatient(fhir_resource_patient, tryton_conection)
        Patient.save(new_patient)
        return new_patient
    # el paciente existe actualizo su du
    else:
        du = FHIR2GNUDu(fhir_resource_patient, tryton_conection)
        patient_update = patient[0] 
        patient_update.name.du = du.id
        patient_update.name.save()
        return patient_update

#
def FHIR2GNUPatient(fhir_resource_patient, tryton_conection):
    # create du 
    du = FHIR2GNUDu(fhir_resource_patient, tryton_conection)

    Patient = tryton_conection.pool.get('gnuhealth.patient')
    Party = tryton_conection.pool.get('party.party')
    Contact = tryton_conection.pool.get('party.contact_mechanism')

    party = Party.create([{
                'ref': fhir_resource_patient['identifier'][1]['value'],
                'name': fhir_resource_patient['name'][0]['given'][0],
                'lastname': fhir_resource_patient['name'][0]['family'],
                'dob': datetime.strptime(fhir_resource_patient['birthDate'],'%d/%m/%Y').date(),#'%d/%m/%Y'
                'gender': 'm' if fhir_resource_patient['gender'] == 'male' else 'f',
                'du': du.id,
                'is_patient': True,
                'is_person': True,
                'fed_country': fhir_resource_patient['address'][0]['country'],
                'addresses': [('create',[{
                    'street': fhir_resource_patient['address'][0]['line'][0],
                    #'country': 32,
                    #'subdivision': 'Salta',
                    }])]
    }])

    contact = Contact.create([{
        'type':'phone',
        'value': fhir_resource_patient['telecom'][0]['value'],
        'party': party[0].id
        }])

    patient = Patient.create([{
                'name': party[0].id
    }])

    return patient

#
def FHIR2GNUDu(fhir_resource_patient, tryton_conection):
    latitude = Decimal(fhir_resource_patient['address'][0]['extension'][0]['extension'][0]['valueDecimal']).quantize(Decimal('1.11111111111111'), rounding=ROUND_UP)
    longitude = Decimal(fhir_resource_patient['address'][0]['extension'][0]['extension'][1]['valueDecimal']).quantize(Decimal('1.11111111111111'), rounding=ROUND_UP)
    
    if fhir_resource_patient['identifier'][2]['value'] == 'none':
        str_code_du = code_du(  fhir_resource_patient['address'][0]['line'][0],
                            fhir_resource_patient['address'][0]['extension'][1]['extension'][0]['valueString'],
                            fhir_resource_patient['name'][0]['family'])
    else:
        str_code_du = fhir_resource_patient['identifier'][2]['value']
        
    Country = tryton_conection.pool.get('country.country')
    country = Country.search([('code','=',fhir_resource_patient['address'][0]['country'])])
    Subdivision = tryton_conection.pool.get('country.subdivision')
    subdivision = Subdivision.search([('code','=','AR-E')])

    DU = tryton_conection.pool.get('gnuhealth.du')

    # compruebo si existe alguna du con ese nombre
    dus_name = DU.search([('name','=',str_code_du)])

    if len(dus_name) != 0:
        dus_name[0].latitude = latitude
        dus_name[0].longitude = longitude
        dus_name[0].address_country = country[0].id 
        dus_name[0].address_subdivision = subdivision[0].id
        dus_name[0].save()
        return dus_name[0]
    else:
        du = DU.create([{
                    'name': str_code_du,
                    'address_street': fhir_resource_patient['address'][0]['line'][0],
                    'address_street_number': fhir_resource_patient['address'][0]['extension'][1]['extension'][0]['valueString'],
                    'address_city': fhir_resource_patient['address'][0]['city'],
                    'address_country': country[0].id,
                    'address_subdivision': subdivision[0].id,
                    'latitude': latitude,
                    'longitude': longitude,
                    }])
        DU.save(du)
        return du[0]

def create_housing_condition(fhir_resource_patient, tryton_conection):
    with open('data.json') as feedsjson:
        feeds = json.load(feedsjson)

    items = []
    dates = []

    items = search_resources(fhir_resource_patient, 'Observation')
    dates += [item['effectiveDateTime'] for item in items]  
    dates = list(dict.fromkeys(dates))

    if fhir_resource_patient['identifier'][2]['value'] == 'none':
        str_code_du = code_du(  fhir_resource_patient['address'][0]['line'][0],
                                fhir_resource_patient['address'][0]['extension'][1]['extension'][0]['valueString'],
                                fhir_resource_patient['name'][0]['family'])
    else:
        str_code_du = fhir_resource_patient['identifier'][2]['value']

    if len(items) == 0:
        return None
    else:
        DU = tryton_conection.pool.get('gnuhealth.du')
        dus = DU.search([('name','=',str_code_du)])
        if len(dus) != 0:
            # Agrupo los datos de las diferentes observaciones de una misma fecha
            data_observation = {}
            for date in dates:
                data_observation = {}
                for item in items:
                    if item['effectiveDateTime'] == date:
                        #print('antes del error', item['code']['coding'][0]['code'])
                        #print('antes del error', item['valueCodeableConcept']['coding'][0]['code'])
                        data_observation[item['code']['coding'][0]['code']] = item['valueCodeableConcept']['coding'][0]['code']

                print(data_observation)
                HealthConditions = tryton_conection.pool.get('gnuhealth.housing')

                if 'ELECTRICIDAD' in data_observation:
                    electricity = 'si' if data_observation['ELECTRICIDAD'] == 'SI' else 'no'
                else:
                    electricity = 'sin_dato'

                print("UD", dus[0].name)
                
                healthconditions = HealthConditions.create([{
                            'du': dus[0].id,
                            'revision_date': datetime.strptime(date, '%Y/%m/%d').date(), #'%Y-%m-%d').date(),
                            'housing_type': map2gnuIndecCodes(data_observation['TIPO DE VIVIENDA'], 'TIPO DE VIVIENDA') if 'TIPO DE VIVIENDA' in data_observation else 'sin_dato',
                            'housing_type_another': data_observation['TIPO DE VIVIENDA'] if map2gnuIndecCodes(data_observation['TIPO DE VIVIENDA'], 'TIPO DE VIVIENDA') == 'another' else '',
                            'water': 'potable', #if data_observation['ORIGEN AGUA'] == 'RED PUBLICA' else 'sin_dato', #completAR Y REVISAR CATEGORIA
                            'water_location': map2gnuIndecCodes(data_observation['AGUA'], 'AGUA') if 'AGUA' in data_observation else 'no_data',
                            'water_source':map2gnuIndecCodes(data_observation['ORIGEN AGUA'], 'ORIGEN AGUA') if 'ORIGEN AGUA' in data_observation else 'no_data',
                            'water_source_other': data_observation['ORIGEN AGUA'] if map2gnuIndecCodes(data_observation['ORIGEN AGUA'], 'ORIGEN AGUA') == 'other' else '',
                            'bathroom': map2gnuIndecCodes(data_observation['BAÑO'],'BAÑO') if 'BAÑO' in data_observation  else 'no_data',
                            'bathroom_type': map2gnuIndecCodes(data_observation['EL BAÑO TIENE'],'EL BAÑO TIENE') if 'EL BAÑO TIENE' in data_observation  else 'no_data',
                            'snow_ice': 'no_data', #hacer
                            'excretes': map2gnuIndecCodes(data_observation['EXCRETAS'],'EXCRETAS') if 'EXCRETAS' in data_observation  else 'sin_dato',
                            'disposal':'sin_dato', # no contemplado en relevar
                            'walls': map2gnuIndecCodes(data_observation['MATERIAL PREDOMINANTE EN LAS PAREDES EXTERIORES'],'MATERIAL PREDOMINANTE EN LAS PAREDES EXTERIORES') if 'MATERIAL PREDOMINANTE EN LAS PAREDES EXTERIORES' in data_observation  else 'sin_dato',
                            'walls_else': data_observation['MATERIAL PREDOMINANTE EN LAS PAREDES EXTERIORES'] if map2gnuIndecCodes(data_observation['MATERIAL PREDOMINANTE EN LAS PAREDES EXTERIORES'],'MATERIAL PREDOMINANTE EN LAS PAREDES EXTERIORES') == 'otro' else '',
                            'roofs': map2gnuIndecCodes(data_observation['MATERIAL PREDOMINANTE EN LA CUBIERTA EXTERIOR DEL TECHO'],'MATERIAL PREDOMINANTE EN LA CUBIERTA EXTERIOR DEL TECHO') if 'MATERIAL PREDOMINANTE EN LA CUBIERTA EXTERIOR DEL TECHO' in data_observation  else 'sin_dato',
                            'roof_else': data_observation['MATERIAL PREDOMINANTE EN LA CUBIERTA EXTERIOR DEL TECHO'] if map2gnuIndecCodes(data_observation['MATERIAL PREDOMINANTE EN LA CUBIERTA EXTERIOR DEL TECHO'],'MATERIAL PREDOMINANTE EN LA CUBIERTA EXTERIOR DEL TECHO') == 'otro' else '',
                            'floors': map2gnuIndecCodes(data_observation['MATERIAL DE LOS PISOS'],'MATERIAL DE LOS PISOS') if 'MATERIAL DE LOS PISOS' in data_observation  else 'sin_dato',
                            'floor_else': data_observation['MATERIAL DE LOS PISOS'] if map2gnuIndecCodes(data_observation['MATERIAL DE LOS PISOS'],'MATERIAL DE LOS PISOS') == 'otros' else '',
                            'housemates': data_observation['HABITANTES'] if 'HABITANTES' in data_observation else 0,
                            'minors': data_observation['MENORES'] if 'MENORES' in data_observation else 0,
                            'gas': 'sin_dato', #revisar el mapeo
                            'stove_feed': map2gnuIndecCodes(data_observation['USA PARA COCINAR'],'USA PARA COCINAR') if 'USA PARA COCINAR' in data_observation  else 'no_data',
                            'kitchen': 'sin_dato', #resolver mapeo
                            'electricity': electricity,
                            'bedrooms': data_observation['CANTIDAD DE PIEZAS'] if 'CANTIDAD DE PIEZAS' in data_observation else 0
                }])
                print("antes de enviar", healthconditions)
                HealthConditions.save(healthconditions)
                return healthconditions


def map2gnuIndecCodes(value_code, category):
    # Boton Inspeccion Exterior -------------------------------------------------------------------------------
    # Tipo de vivienda particular (V01)
    if category == 'TIPO DE VIVIENDA':
        type1 = ['CASA', 'DEPARTAMENTO', 'PENSION']
        type2 = ['LOCAL NO CONSTRUIDO PARA VIVIENDA']
        type3 = ['CASILLA','RANCHO']
        type4 = ['VIVIENDA MOVIL','PERSONA VIVIENDO EN LA CALLE']
        
        value = '1' if value_code in type1 else \
                '2' if value_code in type2 else \
                '3' if value_code in type3 else 'another'

    elif category == 'MATERIAL PREDOMINANTE EN LAS PAREDES EXTERIORES':
        value = 'ladrillo' if value_code == 'LADRILLO, PIEDRA, BLOQUE, HORMIGON' else\
                'barro' if value_code == 'ADOBE' else\
                'chapa' if value_code == 'CHAPA DE METAL O FIBROCEMENTO' else\
                'madera' if value_code == 'MADERA' else\
                'paja' if value_code == 'CHORIZO, CARTON, PALMA, PAJA SOLA O MATERIAL DE DESECHO' else 'otro' 

    elif category == 'MATERIAL PREDOMINANTE EN LA CUBIERTA EXTERIOR DEL TECHO':
        value = 'chapa' if value_code == 'CHAPA DE METAL' else\
                'paja' if value_code == 'CHAPA DE CARTON, CAÑA, PALMA, TABLA CON BARRO, PAJA CON BARRO O PAJA SOLA' else\
                'ladrillo' if value_code == 'LOZA O CARPETA A LA VISTA (SIN CUBIERTA)' else 'otro'

    # Boton Servicios Basicos ----------------------------------------------------------------------------------

    # Procedencia del agua para beber y cocinar (H09)
    elif category == 'ORIGEN AGUA':
        value = 'public_grid' if value_code == 'RED PUBLICA' else\
                'motor_pump' if value_code == 'PERFORACION CON BOMBA A MOTOR' else\
                'manual_pump' if value_code == 'PERFORACION CON BOMBA MANUAL' else\
                'well_no_pump' if value_code == 'POZO SIN BOMBA' else 'other'

    # Tenencia de agua (H08)
    elif category == 'AGUA':
        value = 'pipes_inside_house' if value_code == 'POR CAÑERIA DENTRO DE LA VIVIENDA' else\
                'outside_house_inside_land' if value_code == 'FUERA DE LA VIVIENDA PERO DENTRO DEL TERRENO' else 'outside_land'# if water_location == 'FUERA DEL TERRENO' 

    # Excretas
    elif category == 'EXCRETAS':
        value = 'cloacas' if value_code == 'A RED PUBLICA O CLOACA' else\
                'letrina1' if value_code == 'A CAMARA SEPTICA Y POZO CIEGO' else\
                'letrina2' if value_code == 'SOLO A POZO CIEGO' else 'letrina3' # if excretes == 'A HOYO, EXCAVACION EN LA TIERRA' 

    # Boton Vivienda -------------------------------------------------------------------------------------------
   
    # Baño
    elif category == 'BAÑO':
        value = 'inside_house' if value_code == 'DENTRO DE LA VIVIENDA' else\
                'outside_house' if value_code == 'FUERA DE LA VIVIENDA' else 'no' 

    # El baño tiene
    elif category == 'EL BAÑO TIENE':
        value = 'toilet_w_button' if value_code == 'INODORO CON BOTON, MOCHILA O CADENA' else\
                'toilet_wo_button' if value_code == 'INODORO SIN BOTON O SIN CADENA' else 'letrine'# if value_code == 'POZO'

    # Usa para cocinar
    elif category == 'USA PARA COCINAR':
        value = 'natural' if value_code == 'GAS DE RED' else\
                'envasado' if value_code == 'GAS EN GARRAFA, EN TUBO O A GRANEL' else\
                'grid_gas' if value_code == 'GAS DE RED' else\
                'tubed_gas' if value_code == 'GAS EN GARRAFA, EN TUBO O A GRANEL' else\
                'wood_coal' if value_code == 'LEÑA O CARBON' else 'electricity'# if gas == 'ELECTRICIDAD'

    # Material predominante 1 de los pisos (H05)
    elif category == 'MATERIAL DE LOS PISOS':
        value = 'ceramico' if value_code == 'CERAMICA, BALDOSA, MOSAICO, MADERA O CEMENTO ALISADO' else\
                'cemento' if value_code == 'CARPETA, COMTRAPISO O LADRILLO FIJO' else 'tierra'# if floors == 'TIERRA O LADRILLO SUELTO' else 'otros'

    return value

def code_du(street, number_street, lastname):
    code = "RelevAr" 
    if street != "":
        code += " - " + street.upper() 
    if number_street != "":
        code += " " + number_street 
    if lastname != "":
        code += " (" + lastname.upper() + ")"
    return code            

def create_service(fhir_resource_patient, tryton_conection):
    # Para poder crear una peticion de test, se debe configurar el templete, y el lab test con los sig, datos

    # Configuración de GNU para HPV y Colon -----------------------------------------------------------------
    # Test VPH:
    #   Test: Detección de ADN de virus del Papiloma Humano
    #   Código: 35904009 (SNOMED-CT)
    #   Producto: 
    #       name = Detección de ADN de virus del Papiloma Humano 
    #       tipo = Servicios
    #       Udm = Unidad
    #       Precio de venta = 1

    # Screening Colon-Recto:
    #   Test: Screening de neoplasia maligna de colon
    #   Código: 275978004 (SNOMED-CT)
    #   Producto:
    #       name = Screening de neoplasia maligna de colon
    #       tipo = Servicios
    #       Udm = Unidad
    #       Precio de venta = 1

    # Profesionales
    # Crear los profesionales que realizan la busqueda en terreno, la busqueda se hace por dni

    items = search_resources(fhir_resource_patient, 'ServiceRequest')
    for item in items:
        serviceRequestFHIR2GNU(item, tryton_conection)

def search_resources(fhir_resource_patient, type_resource):
    feeds = []
    with open('data.json') as feedsjson:
        feeds = json.load(feedsjson)

    items = []
    for feed in feeds:
        items += [item['resource'] for item in feed['entry'] if item['resource']['resourceType'] == type_resource]

    values = []
    if len(items) != 0 and type_resource == 'ServiceRequest':
        values += [item for item in items if item['subject']['identifier'] == fhir_resource_patient['identifier'][1]['value']]

    if len(items) !=0 and type_resource == 'Observation':
        latitude = fhir_resource_patient['address'][0]['extension'][0]['extension'][0]['valueDecimal']
        longitude = fhir_resource_patient['address'][0]['extension'][0]['extension'][1]['valueDecimal']
        code_du = str(latitude) + ' ' + str(longitude)
        values += [item for item in items if item['identifier'][0]['value'] == code_du]

    return values

def serviceRequestFHIR2GNU(fhir_resource, tryton_conection):
    Labtest = tryton_conection.pool.get('gnuhealth.lab.test_type')
    Lab = tryton_conection.pool.get('gnuhealth.patient.lab.test')
    Patient = tryton_conection.pool.get('gnuhealth.patient')

    labtest = Labtest.search([('code','=',fhir_resource['code']['values'])])
    patient = Patient.search([('name.ref','=',fhir_resource['subject']['identifier'])])

    date = datetime.strptime(fhir_resource['ocurrenceDateTime']+' 01:55:19','%Y-%m-%d %H:%M:%S')

    lab = Lab.create([{
        'name': labtest[0].id,
        'patient_id': patient[0].id,
        'state': 'ordered',
        'date': date
        }])
    Lab.save(lab)
    return lab

def delete_resource(fhir_resource_patient):
    feeds = []
    with open('data.json') as feedsjson:
        feeds = json.load(feedsjson)

    latitude = fhir_resource_patient['address'][0]['extension'][0]['extension'][0]['valueDecimal']
    longitude = fhir_resource_patient['address'][0]['extension'][0]['extension'][1]['valueDecimal']
    code_du = str(latitude) + ' ' + str(longitude)

    for feed in feeds:
        items = []
        for item in feed['entry']:
            if item['resource']['resourceType'] == 'Patient' and item['resource']['identifier'][0]['value'] != fhir_resource_patient['identifier'][0]['value']:
                items += [item]
            elif item['resource']['resourceType'] == 'Observation' and item['resource']['identifier'][0]['value'] != code_du:
                items += [item]
            elif item['resource']['resourceType'] == 'ServiceRequest' and item['resource']['identifier'][0]['value'] != fhir_resource_patient['identifier'][0]['value']:
                items += [item]

        if len(items) == 0:
            feeds.remove(feed)
        else:
            feed['entry'] = items

    with open('data.json', mode='w') as f:
        f.write(json.dumps(feeds, indent=2))