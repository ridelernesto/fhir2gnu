import os

from flask import Flask, flash, render_template, url_for, request, redirect, session, abort, jsonify, g
from flask_tryton import Tryton
from flask_babel import Babel
from flask_wtf import FlaskForm
from datetime import datetime, date, timedelta
from dateutil.relativedelta import relativedelta
from decimal import Decimal, ROUND_UP

from base64 import b64decode, b64encode
import base64
import urllib.request as urllib2
from Crypto.Cipher import AES
import hashlib
from secrets import token_bytes
from Crypto import Random
from pbkdf2 import PBKDF2
import binascii
from collections import Counter
from Crypto.Hash import SHA256
from Crypto.Util.Padding import pad
from Crypto.Util.Padding import unpad

from functools import wraps

from wtforms import StringField
from wtforms.validators import DataRequired
from wtforms.fields import PasswordField

import time
import json 
from selenium import webdriver
from webdriver_manager.firefox import GeckoDriverManager

#libreria para leer en formato fhir y convertir a recursos gnu
from FHIR2GNUHealth import create_patient, create_housing_condition, code_du, create_service, delete_resource 
#from selenium.common.keys import Keys

app = Flask(__name__)
babel = Babel(app)
app.config['TRYTON_DATABASE'] = 'healthx'
#os.environ.get('pruebas', 'pruebas')
app.config['TRYTON_CONFIG'] = '/home/ernesto/.virtualenvs/healthx/lib/python3.8/site-packages/trytond/etc/trytond.conf'
tryton = Tryton(app, configure_jinja=True)


SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY
turnos_webhook = []
data_resources = []
id_user = 0
tests = {   'HPV':{'solicitado':'NO', 'date':''},
            'COLON':{'solicitado':'NO', 'date':''}}
key = token_bytes(16)
"""WebUser = tryton.pool.get('web.user')
Session = tryton.pool.get('web.user.session')"""

Patient = tryton.pool.get('gnuhealth.patient')

SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY
institution=['']



#----------------------------------------------------------------------------------------------------------
privateKey = "A?D(G+KbPeShVmYq"

# esta ruta recibe los json y los almacena desde RelevAr
@app.route("/json", methods=["GET","POST"])
@tryton.transaction()
def starting_url():
    if request.method == 'POST':
        admin = request.values.get('user')
        passw = request.values.get('password')
        data = request.values.get('data')
        strKey = 'admin1RelevAr202'

        if admin != None and passw != None:
            User = tryton.pool.get('res.user')
            users = User.search([('login','=', admin)])
            if len(users) != 0:
                if User.get_login( admin, {'password': decrypt_aes(passw, privateKey)}) == users[0].id:
                    id_user = str(users[0].id)
                    strKey = generateStrKey(passw, id_user)
                    return str(id_user)  
                else:
                    return 'error_user'
            else:
                return 'error_user'  
        elif data != None:
            data_json = decrypt_aes(data, strKey).decode('utf-8')
            data_json = json.loads(data_json)

            a = []
            if not os.path.isfile('data.json'):
                a.append(data_json)
                with open('data.json', mode='w') as f:
                    f.write(json.dumps(a, indent=2))
            else:
                feeds = []
                with open('data.json') as feedsjson:
                    feeds = json.load(feedsjson)

                feeds.append(data_json)
                with open('data.json', mode='w') as f:
                    f.write(json.dumps(feeds, indent=2))
            return 'decrypt_data'
        else:
            return 'error'
    else: 
        return 'error'

#key = 'areyouokareyouok'
_iv = "\x00"*AES.block_size # creates a 16 byte zero initialized string

def decrypt_aes(cryptedStr, key):
    generator = AES.new(key.encode(), AES.MODE_CBC, _iv.encode())
    cryptedStr_bytes = base64.b64decode(cryptedStr)
    recovery = generator.decrypt(cryptedStr_bytes)
    return unpad(recovery, AES.block_size)

def generateStrKey(passw, id):
    value = passw + id + "RelevAr" + "2020" + "UNER";
    return value[:16];



#--------------------------------------------------------------------------------------------------------------
@app.route('/relevar_login', methods=['GET','POST'])
@tryton.transaction()
def relevar_login():
    if request.method == 'POST':
        session.pop('user', None)

        User = tryton.pool.get('res.user')
        users = User.search([('login','=',request.form['username'])])
        if len(users) !=0 :
            if User.get_login(request.form['username'], {'password': request.form['password']}) == users[0].id:
                session['user'] = request.form['username']
                id_user = users[0].id
                return redirect(url_for('relevar'))   

    return render_template('RelevAr/relevar_login.html')



#-------------------------------------------------------------------------------------------------------------
@app.route('/relevar', methods=['GET','POST'])
@tryton.transaction()
def relevar():
    g.user = None

    if 'user' in session:
        g.user = session['user']

    if g.user:
        feeds = []
        with open('data.json') as feedsjson:
            feeds = json.load(feedsjson)

        items = []
        for feed in feeds:
            items += [item['resource'] for item in feed['entry'] if item['resource']['resourceType'] == 'Patient']

        today = datetime.today().date().strftime('%d %m %Y')
        return render_template('RelevAr/relevar.html',
                                user = session['user'],
                                items = items
                            )
    return redirect(url_for('relevar_login'))


#----------------------------------------------------------------------------------------------------------
@app.route('/relevar_patient', methods=['GET','POST'])
@tryton.transaction()
def relevar_patient():
    g.user = None
    if 'user' in session:
        g.user = session['user']
    if g.user:
        if request.method == 'POST':            
            data = request.get_json(force=True)
            data_resources.append(data)

            tests['HPV']['solicitado'] = 'SI'
            tests['HPV']['date'] = '2022-01-03'

        else:
            return render_template('RelevAr/relevar_patient.html',
                                    user = session['user'],
                                    item = data_resources[-1],
                                    tests = tests)

    return redirect(url_for('relevar_login'))


#-------------------------------------------------------------------------------------------------------------
@app.route('/confirm_du', methods=['GET','POST'])
@tryton.transaction()
def confirm_du():
    g.user = None
    if 'user' in session:
        g.user = session['user']
    if g.user:
        if request.method == 'POST':            
            data = request.get_json(force=True)
            data_resources.append(data)

        else:
            
            DU = tryton.pool.get('gnuhealth.du')

            # compruebo si existe alguna du con ese nombre
            #code_du = data_resources[-1]['address'][0]['line'][0].upper()+" (" + data_resources[-1]['name'][0]['family'].upper() +")"
            str_code_du = ''
            str_code_du = code_du(  data_resources[-1]['address'][0]['line'][0],
                                data_resources[-1]['address'][0]['extension'][1]['extension'][0]['valueString'],
                                data_resources[-1]['name'][0]['family'])
            str_code_du.replace('RelevAr','')
            str_code_du.replace('-','')
            split_code_du = str_code_du.split(" ")

            dus_name = []
            for code in split_code_du:
                dus_name += DU.search([('name','like','%'+code+'%')])
            
            latitude = Decimal(data_resources[-1]['address'][0]['extension'][0]['extension'][0]['valueDecimal']).quantize(Decimal('1.11111111111111'), rounding=ROUND_UP)
            longitude = Decimal(data_resources[-1]['address'][0]['extension'][0]['extension'][1]['valueDecimal']).quantize(Decimal('1.11111111111111'), rounding=ROUND_UP)
            dus_name += DU.search([('latitude','=',latitude),('longitude','=',longitude)])
            
            dus_name = list(dict.fromkeys(dus_name))
            
            items = []
            for du in dus_name:
                members = []
                for member in du.members:
                    members+=[member.name.upper()+' '+member.lastname.upper()]
                items += [{'code':du.name, 'members':members}]
            items += [{'code':'LA UD NO ESTA EN LA LISTA', 'members':[]}]

            return render_template('RelevAr/confirm_du.html',
                                        user = session['user'],
                                        items = items,
                                        patient = data_resources[-1]
                                        )

    return redirect(url_for('relevar_login'))


#---------------------------------------------------------------------------------------------------------------
@app.route('/save_data', methods=['GET','POST'])
@tryton.transaction(user=id_user, readonly=False)
def save_data():  
    data = request.get_json(force=True)
    data_resources.append(data)
    patient = create_patient(data_resources[-1], tryton)  
    print(patient)
    
    house_con = create_housing_condition(data_resources[-1], tryton)
    print('housing cond',house_con)

    create_service(data_resources[-1], tryton)

    delete_resource(data_resources[-1])
    return "OK"

if __name__ == "__main__":
    app.run(host="192.168.0.108",debug=True)
    #app.run(host="192.168.1.111",debug=True)
    #app.run(debug=True)

